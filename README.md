Projekt Erlang - dbg
Podczas tworzenia prezentacji opieraliśmy się na http://www.erlang-factory.com/upload/presentations/316/dbg%5B1%5D.pdf.
Uznaliśmy, że należy przedstawić grupie dwa mechanizmy opisane w powyższej prezentacji tj. dbg introduction and basics oraz match specification. 
Jednak obawialiśmy się, że zabraknie na to wszystko czasu. Postanowiliśmy utworzyć prezentację dotyczącą wprowadzenia do narzędzia dbg, zaprezentować kilka przykładów 
a następnie jeśli wystarczyłoby czasu przedstawić zagadnienia związane z tematem match specification. 
