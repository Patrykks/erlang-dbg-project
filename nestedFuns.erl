-module(nestedFuns).
-export([start/0, init/0, stop/1, func/2]).

start() ->
	spawn(?MODULE, init, []).

stop(Pid) ->
	Pid ! {Pid, stop}.
	
func(Pid, A) ->
	Pid ! {self(), A},
	receive
		V -> V
	after 1000 ->
		ok
	end.
	
init() ->
	loop().
	
loop() ->
	receive
		{_, stop} -> ok;
		{Pid, A} ->
			Pid ! funA(A),
			spawn(?MODULE, funA, [4]),
			loop()
	end.

funA(A) -> funB(A+1, A+2).
funB(A, B) -> funC(A+B, A).
funC(A, B) -> funD(A+A, B+B, A+B).
funD(A, B, C) -> (A-B+1)/C + (A-C+1)/B + (B-C+1)/A.