-module(factorial).
-export([start/0, loop/0, factUser/1]).

start() -> 
	register(factServer, spawn(?MODULE, loop, [])).
	
factUser(N) ->
	factServer ! {self(), N},
	receive
		A -> A
	after 1000
		-> {error, timed_out}
	end.

loop() ->
	receive
		{_, stop} -> ok;
		{Pid, N} -> Pid ! factorial(N),
		loop()
	end.

factorial(N) when N > 0 -> N*factorial(N-1);
factorial(0) -> 1.